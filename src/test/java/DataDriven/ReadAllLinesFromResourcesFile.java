package DataDriven;

import Tests.TestLogin_DataDriven;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class ReadAllLinesFromResourcesFile {
    public static List<String> readAllLinesFromResourcesFile(String fileName) throws IOException {
        List<String> result = new ArrayList<>();
        ClassLoader classLoader = TestLogin_DataDriven.class.getClassLoader();
        URL resource = classLoader.getResource(fileName);
        File file ;
        if (resource == null) {
            throw new IllegalArgumentException("file not found!");
        } else {
            file = new File(resource.getFile());
        }

        try (FileReader reader = new FileReader(file);
             BufferedReader br = new BufferedReader(reader)) {

            String line;
            while ((line = br.readLine()) != null) {
                result.add(line);
            }
        }
        return result;
    }
}
