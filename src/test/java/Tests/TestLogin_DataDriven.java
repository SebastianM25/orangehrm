package Tests;

import Base.BaseClass;
import DataDriven.ReadAllLinesFromResourcesFile;
import Page.Login;
import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertTrue;

@RunWith(DataProviderRunner.class)
public class TestLogin_DataDriven extends BaseClass {


    @Test
    public void validLogin() {
        String username = "Admin";
        String password = "admin123";
        Login login = new Login(driver);
        login.fillUsername(username);
        login.fillPassword(password);
        login.clickLogin();
        assertTrue(driver.getPageSource().contains("Welcome Admin"));
    }

    @UseDataProvider("power")
    @Test
    public void multipleInputsLogin(String username, String password) {

        Login login = new Login(driver);
        login.fillUsername(username);
        login.fillPassword(password);
        login.clickLogin();
        assertTrue(driver.getPageSource().contains("Welcome Admin"));
    }

    @DataProvider()
    public static Object[][] power() throws IOException {
        List<Object[]> outputList = new ArrayList<>();
        ReadAllLinesFromResourcesFile obj = new ReadAllLinesFromResourcesFile();
        List<String> records = ReadAllLinesFromResourcesFile.readAllLinesFromResourcesFile("DataDriven/Credentials.csv");
        for (String record : records) {

            String[] params = record.split(",");
            outputList.add(params);
        }
        Object[][] result = new Object[records.size()][];
        outputList.toArray(result);
        return result;
    }
}
