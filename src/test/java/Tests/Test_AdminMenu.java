package Tests;

import Base.BaseClass;
import Page.AdminPage.AdminPage;
import Page.Login;
import org.junit.Assert;
import org.junit.Test;
import utilities.Waits;

import java.util.concurrent.TimeUnit;


public class Test_AdminMenu extends BaseClass {

    @Test
    public void testAdminMenu() throws InterruptedException {
        String employeeName="Linda Anderson";
        String Username="Emmanuela";
        String passwordUser="123456789Se!@";
        String confirmPasswordUser="123456789Se!@";
        Login login = new Login(driver);
        login.LoginValid();
        AdminPage adminPage = new AdminPage(driver);
        adminPage.clickOnTheAddUser();
        adminPage.selectUserRole();
        adminPage.userRoleDropDown();
        adminPage.fillEmployeeName(employeeName);
        adminPage.fillUsername(Username);
        adminPage.statusDropDown();
        adminPage.fillPassword(passwordUser);
        adminPage.fillConfirmPassword(confirmPasswordUser);
        adminPage.clickOnTheSaveBtn();
        navigateTo();
        Thread.sleep(5000);
        Assert.assertTrue(driver.getPageSource().contains("Raul de San Sebastian"));

    }
    @Test
    public void testAdminMenu_2() throws InterruptedException {
        String employeeName="Linda Anderson";
        String Username="Emmanuela";
        String passwordUser="123456789Se!@";
        String confirmPasswordUser="123456789Se!@";
        Login login = new Login(driver);
        login.LoginValid();
        AdminPage adminPage = new AdminPage(driver);
        adminPage.clickOnTheAddUser();
        adminPage.selectUserRole();
        adminPage.userRoleDropDown();
        adminPage.fillEmployeeName(employeeName);
        adminPage.fillUsername(Username);
        adminPage.statusDropDown();
        adminPage.fillPassword(passwordUser);
        adminPage.fillConfirmPassword(confirmPasswordUser);
        adminPage.clickOnTheSaveBtn();
        navigateTo();
        Thread.sleep(5000);
        Assert.assertTrue(driver.getPageSource().contains("Raul de San Sebastian"));

    }
    @Test
    public void loginValid() throws InterruptedException {
        Login login = new Login(driver);
        login.LoginValid();
        login.hoverAdminMeniu();
        login.clickAdminMeniu();
    }
    public void navigateTo(){
        driver.manage().timeouts().pageLoadTimeout(3,TimeUnit.SECONDS);
        driver.navigate().to("https://opensource-demo.orangehrmlive.com/index.php/admin/viewSystemUsers");
    }
}
