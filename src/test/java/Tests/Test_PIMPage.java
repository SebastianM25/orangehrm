package Tests;

import Base.BaseClass;
import Page.Login;
import Page.PIM;
import org.junit.Assert;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

public class Test_PIMPage extends BaseClass {
    @Test
    public void employeeList() throws InterruptedException {

        Login login = new Login(driver);
        login.LoginValid();
        PIM pim = new PIM(driver);
        pim.hoverPim();
        pim.clickPim();
        pim.clickOnAdd();
        Thread.sleep(5000);
        assertTrue("Add Employee", driver.getPageSource().contains("Add Employee"));

    }
}
