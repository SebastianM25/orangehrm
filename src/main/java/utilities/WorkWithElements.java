package utilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class WorkWithElements {
    public static void clickElement(WebElement element) throws InterruptedException {
        validation(element);
        element.click();
        Thread.sleep(100);
    }

    public static void complete(WebElement element, String wanteddata) throws InterruptedException {
        validation(element);
        element.sendKeys(wanteddata);
    }

    public static boolean validation(WebElement element) {
        Waits.waitForElement(element);
        return element.isEnabled();
    }

    public static boolean checkElementWords(WebElement element, String checkWord) throws InterruptedException {
        Thread.sleep(100);
        String text = element.getText();
        return (text.contains(checkWord));
    }
}
