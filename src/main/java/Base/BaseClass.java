package Base;

import org.apache.commons.lang3.SystemUtils;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;

public class BaseClass {
    public WebDriver driver;
    String url = "https://opensource-demo.orangehrmlive.com/";

    private DriversUtils driversPath = new DriversUtils();

    /* @Before
     public void setUpTestChrome() {
         System.setProperty("webdriver.chrome.driver", driversPath.getDriverDirPath() + "chromedriver" + driversPath.getDriverExtension());
         driver = new ChromeDriver();

         driver.get(url);
         if (SystemUtils.IS_OS_WINDOWS) {
             driver.manage().window().maximize();
         }
     }*/
    @Before
    public void setUpTestFireFox() {

        System.setProperty("webdriver.gecko.driver", driversPath.getDriverDirPath() + "geckodriver" + driversPath.getDriverExtension());
        driver = new FirefoxDriver();
        driver.get(url);
        if (SystemUtils.IS_OS_WINDOWS) {
            driver.manage().window().maximize();
        }
    }
    /*public void waitTime(int timeToWaitInMilliseconds) {
        try {
            Thread.sleep(timeToWaitInMilliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }*/

    @After
    public void tearDown() {
        driver.quit();
    }
}
