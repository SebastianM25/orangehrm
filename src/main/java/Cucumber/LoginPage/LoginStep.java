package Cucumber.LoginPage;

import Base.BaseClass;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class LoginStep  {
    WebDriver driver;
    @Given("I launch firefox browser")
    public void iLaunchFirefoxBrowser() {
        driver= new FirefoxDriver();
    }

    @When("I open orange hrm homepage")
    public void iOpenOrangeHrmHomepage() {
        driver.get("https://s2.demo.opensourcecms.com/orangehrm/symfony/web/index.php/auth/login");
    }

    @Then("I verify that the logo present on page")
    public void iVerifyThatTheLogoPresentOnPage() {
       boolean logo= driver.findElement(By.id("divLogo")).isDisplayed();
       assertEquals(true, logo);
    }

    @And("Close browser")
    public void closeBrowser() {
        driver.close();
    }
}
