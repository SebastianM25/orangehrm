package Page.AdminPage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

public class AdminPage {
    WebDriver driver;

    public AdminPage(WebDriver driver) {
        this.driver = driver;
    }

    private static final By addUser = By.id("btnAdd");
    private static final By userRole = By.id("systemUser_userType");
    private static final By emplName = By.id("systemUser_employeeName_empName");
    public static final By username = By.id("systemUser_userName");
    public static final By password = By.id("systemUser_password");
    public static final By confirmPassword = By.id("systemUser_confirmPassword");
    public static final By saveBtn = By.id("btnSave");


    public void clickOnTheAddUser() {
        driver.findElement(addUser).click();
    }

    public void selectUserRole() {
        driver.findElement(userRole).click();
    }

    public void fillEmployeeName(String employeeName) {
        driver.findElement(emplName).sendKeys(employeeName);
    }

    public void fillUsername(String Username) {
        driver.findElement(username).sendKeys(Username);
    }

    public void fillPassword(String passwordUser) {
        driver.findElement(password).sendKeys(passwordUser);
    }

    public void fillConfirmPassword(String confirmPasswordUser) {
        driver.findElement(confirmPassword).sendKeys(confirmPasswordUser);
    }

    public void clickOnTheSaveBtn() {
        driver.findElement(saveBtn).click();
    }

    public void userRoleDropDown() {
        Select select = new Select(driver.findElement(By.id("systemUser_userType")));
        select.selectByVisibleText("ESS");
    }

    public void statusDropDown() {
        Select select = new Select(driver.findElement(By.id("systemUser_status")));
        select.selectByVisibleText("Enabled");
    }


}
