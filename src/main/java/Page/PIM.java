package Page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class PIM {
    WebDriver driver;
    private final static By pimPage = By.xpath("//b[contains(text(),'PIM')]");
    private final static By addBtn = By.xpath("//input[@id='btnAdd']");

    public PIM(WebDriver driver) {
        this.driver = driver;
    }

    public void clickPim() {
        driver.findElement(pimPage).click();
    }

    public void clickOnAdd() {
        driver.findElement(addBtn).click();
    }

    public void hoverPim() {
        Actions actions = new Actions(driver);
        WebElement hoverPIM = driver.findElement(pimPage);
        actions.moveToElement(hoverPIM).perform();
    }
}
