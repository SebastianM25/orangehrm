package Page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;


public class Login {

    private static final By usernameField = By.id("txtUsername");
    private static final By passwordField = By.id("txtPassword");
    private static final By loginButton = By.id("btnLogin");
    private static final By adminMenu = By.id("menu_admin_viewAdminModule");

    WebDriver driver;

    public Login(WebDriver driver) {
        this.driver = driver;
    }

    public void fillUsername(String username) {
        driver.findElement(usernameField).sendKeys(username);
    }

    public void fillPassword(String password) {
        driver.findElement(passwordField).sendKeys(password);
    }

    public void clickLogin() {
        driver.findElement(loginButton).click();
    }

    public void hoverAdminMeniu() {
        Actions actions = new Actions(driver);
        WebElement hoverAdminView = driver.findElement(adminMenu);
        actions.moveToElement(hoverAdminView).perform();
    }

    public void clickAdminMeniu() {
        driver.findElement(adminMenu).click();
    }

    public void LoginValid() throws InterruptedException {

        fillUsername("Admin");
        fillPassword("admin123");
        clickLogin();
        Thread.sleep(5000);

    }

}
